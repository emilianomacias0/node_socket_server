const { io } = require('../server');
const { TicketControl } = require('../classes/ticket-control');

const ticketControl = new TicketControl();

io.on('connection', (client) => {

    client.on('siguienteTicket', (data, callback) => {

        let siguinte = ticketControl.siguiente();
        console.log(siguinte);
        callback(siguinte);
    });

    //==================================
    // Emitir ultimo ticket
    //==================================
    client.emit('estadoActual', {
        actual: ticketControl.getUltimoTicket(),
        ultimos4: ticketControl.getUltimos4()
    })



    client.on('atenderTicket', (data, callback) => {
        if (!data.escritorio) {
            return callback('No hay tickets');
        }

        let atenderTicket = ticketControl.atenderTicket(data.escritorio);

        callback(atenderTicket);
        //==================================
        // Actualiza pantalla ultimos 4
        //==================================
        client.broadcast.emit('ultimos4', {
            ultimos4: ticketControl.getUltimos4()
        });
    })
});
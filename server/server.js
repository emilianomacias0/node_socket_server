const express = require('express');
const socketIO = require('socket.io');
const http = require('http');

const path = require('path');

const app = express();
let server = http.createServer(app);
const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;
const WebSocket = require('ws');
app.use(express.static(publicPath));

const wss = new WebSocket.Server({server});

let contador = 0;

wss.on('connection', function connection(client) {
    client.on('message', function incoming(counter) {
        console.log("Recibiendo.. "+counter);
        if(counter == 'increment'){
            contador++;
        }else if(counter == 'decrement'){
            contador--;
        }

        /*if(counter) {
            if (!isNaN(counter)) {
                counter = parseInt(counter);
                counter++;
            } else {
                counter = 0;
            }
        }else{
            counter = 0;
        }*/
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                client.send(contador+'');
            }
        });

    });

    client.on('saludo',function saludo(incoming){
        console.log('Incoming ',incoming);
    });

    client.on('close', function close(ws) {
        console.log('Bye bye close');
    });

    //client.send('Conecction succefull!');
});


// IO = esta es la comunicacion del backend
module.exports.io = socketIO(server);

require('./sockets/socket');
//require('./sockets/ws');
//require('./sockets/expressWs');




server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});